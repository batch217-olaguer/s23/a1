console.log("Gotta Catch them All!");

// Pokemon Trainer;

let pokemonTrainer = {
	name: {
		firstName: "Ashiong",
		lastName: "Salongga",
	},
	age: 35, 
	friends: {
		Hoenn: ["May", "Max"],
		Kanto: ["Brock", "Misty"],
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function (chosenPokemon) {
		if (chosenPokemon === "Pikachu") {
			console.log("Pikachu! I chose you!");
		}
		else if (chosenPokemon === "Charizard") {
			console.log("Let's go Charizard!");
		}
		else if (chosenPokemon === "Squirtle") {
			console.log("Spray 'em Squirtle!");
		}
		 else if (chosenPokemon === "Bulbasaur") {
			console.log("Bulbasaur, get ready!");
		} else {
			console.log(chosenPokemon + ", let's do this!");
		}
	},

}

// ===============================================================

let Pokemon = ["Pikachu", "Mewtwo", "Geodude", "Charizard", "Squirtle", "Bulbasaur"];
	// -------------------------PIKACHU--------------------------------
let Pikachu = {
	pokedexID: "025",
	name: "Pikachu", //plan for nicknames ---- not prio.
	level: Lvl = 23, //same sa health
	health: Hlth = (50 + (this.Lvl * 2.2)), // same lang sa attack. and yes po, from the bottom ko po ako nag start sa pag type ng comments.
	attack: (Hlth*.12 + ((this.Lvl + 1) * .6)), // chamba muna ng values na hindi OP and damage.
	type: ["Electric"], // plan to add balance cylce of weaknesses depending on types.
	weaknesses: ["Ground"], // plan to add CRITICAL STRIKE sa mga countered nya.
	abilities: { // variety of abilities for the 3 pokemons in the activity.
		tackle: function () {
			console.log(this.name + " used TACKLE on " + target_pokemon + ".");
			console.log("Tackle is not very effective.");
		},
		quickAttack: function () {
			console.log(this.name + " used QUICK ATTACK on " + target_pokemon + ".");
			console.log("Quick Attack is not very effective.");
		},
		ironTail: function () {
			console.log(this.name + " used IRON TAIL on " + target_pokemon + ".");
			console.log("Iron Tail is not very effective.");
		},
		thunderbolt: function () {
			console.log(this.name + " used THUNDERBOLT on " + target_pokemon + ".");
			console.log("Thunderbolt is not very effective.");
		},
	},
	status: {
		faint: function () {
			console.log(this.name + "fainted!");
		}
	},
}

	// ================================MEWTWO=============================
let Mewtwo = {
	pokedexID: "150",
	name: "Mewtwo", 
	level: Lvl = 100, 
	health: Hlth = 150 + (this.Lvl * 2.2), 
	attack: (Hlth*.12 + ((this.Lvl + 1) * .9)), 
	type: ["Psychic"],
	weaknesses: ["Ghost", "Dark", "Bug"],
	abilities: { 
		pressure: function () {
			console.log(this.name + " used PRESSURE on " + target_pokemon + ".");
			console.log("Pressure is SUPER effective.");
		},
		xBall: function () {
			console.log(this.name + " used X-BALL on " + target_pokemon + ".");
			console.log("X-ball is SUPER effective.");
		},
		psyDrive: function () {
			console.log(this.name + " used PSYDRIVE on " + target_pokemon + ".");
			console.log("Psydrive is SUPER effective.");
		},
		superPsyBolt: function () {
			console.log(this.name + " used SUPER PSYBOLT on " + target_pokemon + "!");
			console.log("Super Psybolt is SUPER effective.");
		},
	},
	status: {
		faint: function () {
			console.log(this.name + "fainted!");
		}
	},
}

	// ===========================GEODUDE================================
let Geodude = {
	pokedexID: "074",
	name: "Geodude", 
	level: Lvl = 12, 
	health: Hlth = 50 + (this.Lvl * 2.2), 
	attack: (Hlth*.12 + ((this.Lvl + 1) * .6)), 
	type: ["Rock", "Ground"],
	weaknesses: ["Water", "Grass", "Ice", "Steel"],
	abilities: { 
		tackle: function () {
			console.log(this.name + " used TACKLE on " + target_pokemon + ".");
			console.log("Tackle is not very effective.");
		},
		rockHead: function () {
			console.log(this.name + " used ROCK HEAD on " + target_pokemon + ".");
			console.log("Rock Head is not very effective.");
		},
		sturdy: function () {
			console.log(this.name + " used STURDY on " + target_pokemon + ".");
			console.log("Sturdy is not very effective.");
		},
	},
	status: {
		faint: function () {
			console.log(this.name + "fainted!");
		},
	},
}

	// ===========================CHARIZARD================================
let Charizard = {
	pokedexID: "006",
	name: "Geodude", 
	level: Lvl = 75, 
	health: Hlth = 50 + (this.Lvl * 2.2), 
	attack: (Hlth*.12 + ((this.Lvl + 1) * .7)), 
	type: ["Fire", "Flying"],
	weaknesses: ["Rock", "Water", "Electric"],
	abilities: { 
		tackle: function () {
			console.log(this.name + " used TACKLE on " + target_pokemon + ".");
			console.log("Tackle is not very effective.");
		},
		rockHead: function () {
			console.log(this.name + " used ROCK HEAD on " + target_pokemon + ".");
			console.log("Rock Head is not very effective.");
		},
		sturdy: function () {
			console.log(this.name + " used STURDY on " + target_pokemon + ".");
			console.log("Sturdy is not very effective.");
		},
	},
	status: {
		faint: function () {
			console.log(this.name + "fainted!");
		},
	},
}

	// ===========================Squirtle================================
let Squirtle = {
	pokedexID: "007",
	name: "Squirtle", 
	level: Lvl = 23, 
	health: Hlth = 50 + (this.Lvl * 2.2), 
	attack: (Hlth*.12 + ((this.Lvl + 1) * .6)), 
	type: ["Water"],
	weaknesses: ["Grass", "Electric"],
	abilities: { 
		tackle: function () {
			console.log(this.name + " used TACKLE on " + target_pokemon + ".");
			console.log("Tackle is not very effective.");
		},
		torrent: function () {
			console.log(this.name + " used ROCK HEAD on " + target_pokemon + ".");
			console.log("Rock Head is not very effective.");
		},
		sturdy: function () {
			console.log(this.name + " used STURDY on " + target_pokemon + ".");
			console.log("Sturdy is not very effective.");
		},
	},
	status: {
		faint: function () {
			console.log(this.name + "fainted!");
		},
	},
}

	// ===========================Squirtle================================
let Bulbasaur = {
	pokedexID: "001",
	name: "Squirtle", 
	level: Lvl = 23, 
	health: Hlth = 50 + (this.Lvl * 2.2), 
	attack: (Hlth*.12 + ((this.Lvl + 1) * .6)), 
	type: ["Grass", "Poison"],
	weaknesses: ["Fire", "Pshychic", "Flying", "Ice"],
	abilities: { 
		tackle: function () {
			console.log(this.name + " used TACKLE on " + target_pokemon + ".");
			console.log("Tackle is not very effective.");
		},
		overgrow: function () {
			console.log(this.name + " used ROCK HEAD on " + target_pokemon + ".");
			console.log("Rock Head is not very effective.");
		},
		sturdy: function () {
			console.log(this.name + " used STURDY on " + target_pokemon + ".");
			console.log("Sturdy is not very effective.");
		},
	},
	status: {
		faint: function () {
			console.log(this.name + "fainted!");
		},
	},
}

